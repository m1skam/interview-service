package interview.web.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@ImportResource("classpath:questions.xml")
@ComponentScan("interview")
@PropertySource({"classpath:application-web-ui.properties"})
public class Starter {
    public static void main(String[] args) {
        run(Starter.class, args);
    }
}