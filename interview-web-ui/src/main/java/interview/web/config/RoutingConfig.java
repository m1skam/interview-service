package interview.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.function.Consumer;

@Configuration
public class RoutingConfig extends WebMvcConfigurerAdapter {
    public static final String LOGIN = "login";
    public static final String INTERVIEW = "interview";
    public static final String RESULTS = "results";

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        setRoot(registry, INTERVIEW);

        Consumer<String> add = view -> registry.addViewController("/" + view).setViewName(view);
        add.accept(LOGIN);
        add.accept(INTERVIEW);
        add.accept(RESULTS);
    }

    private void setRoot(ViewControllerRegistry registry, String page) {
        registry.addViewController("/").setViewName(page);
    }
}