package interview.web.controller;

import interview.answers.AnswerRepository;
import interview.answers.QuestionStatistics;
import interview.answers.User;
import interview.questions.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static interview.web.security.AuthenticationProviderImpl.ADMIN_ROLE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "statistics.do")
@Secured(ADMIN_ROLE)
public class StatisticsController {
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private QuestionRepository questionRepository;

    @RequestMapping(params = "action=rating")
    public List<User> rating() {
        return answerRepository.getUserRating();
    }

    @RequestMapping(params = "action=questionStatistics")
    public QuestionStatistics questionStatistics(int questionNumber) {
        return answerRepository.getQuestionStatistics(questionNumber);
    }

    @RequestMapping(params = "action=reset", method = POST)
    public void reset() {
        answerRepository.reset();
        questionRepository.enableAll();
    }
}