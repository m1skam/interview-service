package interview.web.controller;

import interview.questions.Question;
import interview.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

import static interview.web.security.AuthenticationProviderImpl.ADMIN_ROLE;
import static interview.web.security.AuthenticationProviderImpl.USER_ROLE;
import static interview.web.security.SecurityUtils.getCurrentUser;
import static java.util.Arrays.asList;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "interview.do", method = POST)
@Secured(USER_ROLE)
public class InterviewController {
    @Autowired
    private InterviewService interviewService;

    @RequestMapping(params = "action=current")
    public Question current() {
        return interviewService.current().hideAnswersIfEnabled();
    }

    @RequestMapping(params = "action=myAnswers")
    public Set<Integer> myAnswers(int number) {
        return interviewService.getAnswers(getCurrentUser(), number);
    }

    @RequestMapping(params = "action=answer")
    public boolean answer(int number, @RequestParam("optionNumbers[]") Integer[] optionNumbers) {
        return interviewService.answer(getCurrentUser(), number, new HashSet<>(asList(optionNumbers)));
    }

    @Secured(ADMIN_ROLE)
    @RequestMapping(params = "action=next")
    public void next(int current) {
        interviewService.next(current);
    }

    @Secured(ADMIN_ROLE)
    @RequestMapping(params = "action=prev")
    public void prev(int current) {
        interviewService.prev(current);
    }

    @Secured(ADMIN_ROLE)
    @RequestMapping(params = "action=enable")
    public void enable(int current) {
        interviewService.setEnabled(current, true);
    }

    @Secured(ADMIN_ROLE)
    @RequestMapping(params = "action=disable")
    public void disable(int current) {
        interviewService.setEnabled(current, false);
    }
}