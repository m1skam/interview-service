package interview.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class LoginServiceImpl implements LoginService {
    private final String adminLogin, adminPassword;
    private final Map<String, Integer> loginToPasswordHash = new ConcurrentHashMap<>();

    @Autowired
    public LoginServiceImpl(@Value("${interview.adminLogin}") String adminLogin,
                            @Value("${interview.adminPassword}") String adminPassword) {
        this.adminLogin = adminLogin;
        this.adminPassword = adminPassword;

        autoRegister(adminLogin, adminPassword);
    }

    @Override
    public void autoRegister(String login, String password) {
        loginToPasswordHash.compute(login, (key, initPassword) -> {
            int currentPassword = hash(login, password);
            if (initPassword == null || initPassword == currentPassword) return currentPassword;
            throw new BadCredentialsException("Incorrect login/password");
        });
    }

    @Override
    public boolean isAdmin(String login, String password) {
        return login.equals(adminLogin) && password.equals(adminPassword);
    }

    private int hash(String login, String password) {
        return (login + "|" + password).hashCode();
    }
}