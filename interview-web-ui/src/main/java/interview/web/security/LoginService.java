package interview.web.security;

public interface LoginService {
    void autoRegister(String login, String password);

    boolean isAdmin(String login, String password);
}