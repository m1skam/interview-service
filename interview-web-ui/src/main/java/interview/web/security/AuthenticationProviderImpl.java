package interview.web.security;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.nio.file.DirectoryStream;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;
import static org.springframework.util.StringUtils.hasText;

@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {
    public static final String ADMIN_ROLE = "ROLE_ADMIN";
    public static final String USER_ROLE = "ROLE_USER";

    private final LoginService loginService;

    @Autowired
    public AuthenticationProviderImpl(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        checkLoginAndPassword(authentication);

        String login = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();

        autoRegister(login, password);

        return makeAuthentication(login, loginService.isAdmin(login, password));
    }

    private void checkLoginAndPassword(Authentication authentication) {
        Predicate<Object> notCorrect = str -> str == null || !hasText(str.toString()) || str.toString().length() > 100;

        if (authentication == null || notCorrect.test(authentication.getPrincipal()) || notCorrect.test(authentication.getCredentials())) {
            throw new BadCredentialsException("Empty credentials");
        }
    }

    private void autoRegister(String login, String password) {
        loginService.autoRegister(login, password);
    }

    private Authentication makeAuthentication(String login, boolean isAdmin) {
        Stream<String> roles = isAdmin ? of(ADMIN_ROLE, USER_ROLE) : of(USER_ROLE);
        return new UsernamePasswordAuthenticationToken(login, "", roles.map(SimpleGrantedAuthority::new).collect(toList()));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}