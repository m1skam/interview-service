package interview.answers;

import static java.util.Objects.requireNonNull;

public class User {
    private final String login;
    private final int scores;

    public User(String login, int scores) {
        this.login = requireNonNull(login);
        this.scores = scores;
    }

    public String getLogin() {
        return login;
    }

    public int getScores() {
        return scores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return login.equals(user.login);
    }

    @Override
    public int hashCode() {
        return login.hashCode();
    }

    @Override
    public String toString() {
        return login + " : " + scores;
    }
}