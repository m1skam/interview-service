package interview.answers;

import java.util.List;
import java.util.Set;

public interface AnswerRepository {
    void answer(String user, Answer answer);

    Set<Integer> getAnswers(String currentUser, int number);

    QuestionStatistics getQuestionStatistics(int questionNumber);

    List<User> getUserRating();

    void reset();
}
