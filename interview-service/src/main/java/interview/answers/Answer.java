package interview.answers;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;

public class Answer {
    private final int questionNumber;
    private final Set<Integer> answers;
    private final int scores;

    public Answer(int questionNumber, Set<Integer> answers, int scores) {
        this.questionNumber = questionNumber;
        this.answers = unmodifiableSet(answers);
        this.scores = scores;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public Set<Integer> getAnswers() {
        return answers;
    }

    public int getScores() {
        return scores;
    }

    public static int calcScores(Collection<Answer> answers) {
        return answers.stream().mapToInt(Answer::getScores).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Answer answer = (Answer) o;

        return questionNumber == answer.questionNumber;
    }

    @Override
    public int hashCode() {
        return questionNumber;
    }

    @Override
    public String toString() {
        return questionNumber + " | " + answers + " | " + scores;
    }
}
