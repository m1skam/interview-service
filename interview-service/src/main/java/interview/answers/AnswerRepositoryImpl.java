package interview.answers;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static interview.answers.Answer.calcScores;
import static interview.answers.QuestionStatistics.statisticsOf;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;

@Component
public class AnswerRepositoryImpl implements AnswerRepository {
    private final ConcurrentMap<String, Map<Integer, Answer>> userAnswers = new ConcurrentHashMap<>();

    @Override
    public void answer(String user, Answer answer) {
        this.userAnswers.computeIfAbsent(user, k -> new ConcurrentHashMap<>()).put(answer.getQuestionNumber(), answer);
    }

    @Override
    public Set<Integer> getAnswers(String currentUser, int number) {
        Answer answer = userAnswers.getOrDefault(currentUser, emptyMap()).get(number);
        return answer == null ? emptySet() : answer.getAnswers();
    }

    @Override
    public List<User> getUserRating() {
        return userAnswers.entrySet().stream()
                .map(e -> new User(e.getKey(), calcScores(e.getValue().values())))
                .sorted(comparing(User::getScores).reversed())
                .collect(toList());
    }

    @Override
    public QuestionStatistics getQuestionStatistics(int questionNumber) {
        List<Answer> userAnswers = this.userAnswers.values().stream().map(m -> m.get(questionNumber)).filter(Objects::nonNull).collect(toList());
        return statisticsOf(userAnswers);
    }

    @Override
    public void reset() {
        userAnswers.clear();
    }
}