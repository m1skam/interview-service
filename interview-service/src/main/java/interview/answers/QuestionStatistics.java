package interview.answers;

import java.util.List;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class QuestionStatistics {
    private final int correctCount;
    private final int inCorrectCount;
    private final int allCount;
    private final Map<Integer, Long> optionNumberToCount;
    private final Map<Integer, Long> optionNumberToPercents;

    public static QuestionStatistics statisticsOf(List<Answer> answers) {
        Map<Boolean, Long> statistics = answers.stream().collect(partitioningBy(a -> a.getScores() > 0, counting()));
        int correct = statistics.get(true).intValue();
        int inCorrect = statistics.get(false).intValue();

        Map<Integer, Long> optionNumberToCount = answers.stream().flatMap(a -> a.getAnswers().stream())
                .collect(groupingBy(identity(), counting()));

        return new QuestionStatistics(correct, inCorrect, optionNumberToCount);
    }

    public QuestionStatistics(int correctCount, int inCorrectCount, Map<Integer, Long> optionNumberToCount) {
        this.correctCount = correctCount;
        this.inCorrectCount = inCorrectCount;
        this.allCount = correctCount + inCorrectCount;
        this.optionNumberToCount = optionNumberToCount;

        long optionCount = optionNumberToCount.values().stream().mapToLong(Long::longValue).sum();
        this.optionNumberToPercents = optionNumberToCount.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> Math.round(1d * e.getValue() / optionCount)));
    }

    public int getRightAnswersPercents() {
        return allCount == 0 ? 0 : correctCount * 100 / allCount;
    }

    public int getWrongAnswersPercents() {
        return allCount == 0 ? 0 : 100 - getRightAnswersPercents();
    }

    public int getCorrect() {
        return correctCount;
    }

    public int getInCorrect() {
        return inCorrectCount;
    }

    public Map<Integer, Long> getOptionNumberToCount() {
        return optionNumberToCount;
    }

    public Map<Integer, Long> getOptionNumberToPercents() {
        return optionNumberToPercents;
    }
}