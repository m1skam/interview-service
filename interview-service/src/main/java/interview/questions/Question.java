package interview.questions;

import interview.answers.Answer;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static java.util.Collections.shuffle;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class Question {
    private final int number;
    private final String task;
    private final String text;
    private final List<Option> options;

    private volatile boolean enabled;

    public Question(int number, String task, String text, List<Option> options) {
        this(number, task, text, options, true);
    }

    private Question(int number, String task, String text, List<Option> options, boolean enabled) {
        this.number = number;
        this.task = task;
        this.text = text.replaceAll("\\s+$", "");
        this.options = unmodifiableList(options);
        this.enabled = enabled;
    }

    public int getNumber() {
        return number;
    }

    public String getTask() {
        return task;
    }

    public String getText() {
        return text;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void disable() {
        enabled = false;
    }

    public void enable() {
        enabled = true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Question hideAnswersIfEnabled() {
        if (!enabled) return this;

        List<Option> erased = options.stream().map(Option::erased).collect(toList());
        return new Question(number, task, text, erased, enabled);
    }

    public Answer answer(Set<Integer> optionNumbers) {
        return new Answer(number, optionNumbers, calcScores(optionNumbers));
    }

    private int calcScores(Set<Integer> optionNumbers) {
        Set<Integer> rightAnswers = options.stream().filter(Option::isCorrect).map(Option::getId).collect(toSet());
        return rightAnswers.equals(optionNumbers) ? 1 : 0;
    }

    @Override
    public String toString() {
        return text;
    }
}
