package interview.questions;

public interface QuestionRepository {
    Question getCurrent();

    void makeCurrent(int number);

    void setEnable(int questionNumber, boolean status);

    void enableAll();
}