package interview.questions;

public class Option {
    private final int id;
    private final String text;
    private final boolean correct;

    private Option(int id, String text, boolean correct) {
        this.id = id;
        this.text = text;
        this.correct = correct;
    }

    public static Option correct(int id, String text) {
        return new Option(id, text, true);
    }

    public static Option notCorrect(int id, String text) {
        return new Option(id, text, false);
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean isCorrect() {
        return correct;
    }

    public Option erased() {
        return !correct ? this : new Option(id, text, false);
    }

    @Override
    public String toString() {
        return text;
    }
}
