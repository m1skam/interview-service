package interview.questions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.stream.IntStream.range;

@Component
public class QuestionRepositoryImpl implements QuestionRepository {
    @Autowired
    @Resource(name = "questions")
    private List<Question> questions;
    private volatile int current;

    @Override
    public Question getCurrent() {
        return questions.get(current);
    }

    @Override
    public void makeCurrent(int number) {
        if (number >= 0 && number < questions.size()) {
            current = number;
        }
    }

    @Override
    public void setEnable(int number, boolean status) {
        Question question = questions.get(number);

        if (status) {
            question.enable();
        } else {
            question.disable();
        }
    }

    @Override
    public void enableAll() {
        questions.forEach(Question::enable);
    }

    @PostConstruct
    public void validate() {
        range(0, questions.size()).forEach(i -> {
            Question question = questions.get(i);
            if (question.getNumber() != i) {
                throw new IllegalStateException("Incorrect question number: " + question.getNumber());
            }

            List<Option> options = question.getOptions();
            range(0, options.size()).forEach(j -> {
                Option option = options.get(j);
                if (option.getId() != j) {
                    throw new IllegalStateException("Incorrect option number " + option.getId() + " for question " + question.getNumber());
                }
            });

            if (!options.stream().anyMatch(Option::isCorrect)) {
                throw new IllegalStateException("There are no correct answer for question " + question.getNumber());
            }
        });
    }
}