package interview.questions;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicInteger;

import static interview.questions.Option.correct;
import static interview.questions.Option.notCorrect;
import static java.lang.Integer.parseInt;
import static java.util.Arrays.asList;
import static java.util.Spliterator.ORDERED;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

public class QuestionParser {
    private static final AtomicInteger counter = new AtomicInteger();

    public static Question parse(String text) {
        ListIterator<String> lines = asList(text.split("\n")).listIterator();
        String task = parseTask(lines);
        String question = parseText(lines);
        List<Option> options = getOptions(lines);

        return new Question(counter.getAndIncrement(), task, question, options);
    }

    private static String parseTask(ListIterator<String> lines) {
        while (lines.hasNext()) {
            String line = lines.next().trim();
            if (!line.isEmpty()) return line;
        }
        throw new IllegalStateException();
    }

    private static String parseText(ListIterator<String> lines) {
        StringBuilder text = new StringBuilder();
        while (lines.hasNext()) {
            String line = lines.next();
            if (line.contains("|")) {
                lines.previous();
                break;
            }
            text.append(line).append("\n");
        }

        return text.toString();
    }

    private static List<Option> getOptions(Iterator<String> lines) {
        return stream(spliteratorUnknownSize(lines, ORDERED), false)
                .filter(s -> s.contains("|"))
                .map(String::trim)
                .map(line -> {
                    String[] part = line.split("\\|");
                    String number = part[0];
                    try {
                        return number.contains("+") ?
                                correct(parseInt(number.substring(0, number.length() - 1)), part[1]) :
                                notCorrect(parseInt(number), part[1]);
                    } catch (RuntimeException e) {
                        throw new IllegalArgumentException("Can't parse" + line, e);
                    }
                }).collect(toList());
    }
}