package interview.service;

import interview.answers.Answer;
import interview.answers.AnswerRepository;
import interview.questions.Question;
import interview.questions.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class InterviewServiceImpl implements InterviewService {
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;

    @Autowired
    public InterviewServiceImpl(QuestionRepository questionRepository, AnswerRepository answerRepository) {
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
    }

    @Override
    public Question current() {
        return questionRepository.getCurrent();
    }

    @Override
    public void next(int current) {
        makeCurrent(current + 1);
    }

    @Override
    public void prev(int current) {
        makeCurrent(current - 1);
    }

    @Override
    public boolean answer(String user, int questionNumber, Set<Integer> optionNumbers) {
        Question question = current();
        if (canAnswer(question, questionNumber)) return false;

        Answer answer = question.answer(optionNumbers);
        answerRepository.answer(user, answer);
        return true;
    }

    @Override
    public Set<Integer> getAnswers(String currentUser, int number) {
        return answerRepository.getAnswers(currentUser, number);
    }

    private boolean canAnswer(Question current, int questionNumber) {
        return !current.isEnabled() || current.getNumber() != questionNumber;
    }

    @Override
    public void setEnabled(int questionNumber, boolean status) {
        questionRepository.setEnable(questionNumber, status);
    }

    private void makeCurrent(int number) {
        questionRepository.makeCurrent(number);
    }
}