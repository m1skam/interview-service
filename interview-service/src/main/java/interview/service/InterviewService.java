package interview.service;

import interview.questions.Question;

import java.util.Set;

public interface InterviewService {
    Question current();

    void next(int current);

    void prev(int current);

    boolean answer(String user, int questionNumber, Set<Integer> optionNumbers);

    Set<Integer> getAnswers(String currentUser, int number);

    void setEnabled(int questionNumber, boolean status);
}
